package com.chkris;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.chkris.entity.Product;
import com.chkris.entity.Supplier;
import com.chkris.interaction.UserInteractionContext;
import com.chkris.interaction.processor.CommandProcessor;
import com.chkris.services.SupplierStorageManager;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;


public class JpaMain {

    private  EntityManager entityManager = null;
    private  EntityManagerFactory entityManagerFactory = null;

    public static void main(String[] args) {

        UserInteractionContext context = new UserInteractionContext();
        CommandProcessor commandProcessor = new CommandProcessor();

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try{
            String s = "";
            do {
                context.execute();

                s = bufferRead.readLine();
                // here will be validation

                commandProcessor.process(s);
            } while(! s.equals("exit"));
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
/*


        try {

            entityManager = this.getEntityManager();
            entityManager.getTransaction().begin();

            SupplierStorageManager supplierStorageManager = new SupplierStorageManager(entityManager);



            Supplier supplier = new Supplier();

            Product apple = new Product();
            apple
                .setName("apple")
                .setQuantity(1)
                .setSupplier(supplier);

            Product orangeSet  = new Product();
            orangeSet
                    .setName("orange set")
                    .setQuantity(10)
                    .setSupplier(supplier);

            supplier
                .setName("Polish Kowalsky Supplier")
                .addProduct(apple)
                .addProduct(orangeSet);


            supplierStorageManager.create(supplier);

            supplier.setName("New name");

            supplierStorageManager.update(supplier);



            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
        */
    }

    private EntityManager getEntityManager() {
        if (null == entityManagerFactory) {
            entityManagerFactory = Persistence.createEntityManagerFactory( "com.chkris" );
        }

        if (null == entityManager) {
            entityManager = entityManagerFactory.createEntityManager();
        }

        return entityManager;
    }
}