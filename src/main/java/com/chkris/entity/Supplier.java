package com.chkris.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Supplier implements Model{

    @Id
    @GeneratedValue
    private int id;

    private String name;

    @OneToMany(mappedBy="supplier", cascade= CascadeType.ALL)
    private Set<Product> products;

    public Supplier() {
        this.products = new HashSet<Product>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Supplier setName(String name) {
        this.name = name;

        return this;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public Supplier setProducts(Set<Product> products) {
        this.products = products;

        return this;
    }

    public Supplier addProduct(Product product) {
        this.products.add(product);

        return this;
    }
}