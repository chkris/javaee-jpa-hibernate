package com.chkris.interaction;

public class UserInteractionContext {

    private State current;

    public UserInteractionContext() {
        current = new Welcome();
    }

    public void setState(State state) {
        current = state;
    }

    public void execute() {
        current.next(this);
    }
}
