package com.chkris.interaction;

public class Welcome extends State {

    public static final String WELCOME = "***You are in Product DataStore System***";
    public static final String LIST_OF_OPTIONS = "Below is a list of options you can use: ";
    public static final String SUPPLIER_LIST = "1. Supplier list - command: supplier_list";
    public static final String SUPPLIER_DETAILS = "2. Supplier details with all products - command: supplier_details " +
            "[supplier_id]";
    public static final String PRODUCT_LIST = "3. Product list - command: product_list";
    public static final String CREATE_PRODUCT = "4. Create product for supplier. If supplier doesn't exist error will " +
            "be returned. - command: create_product [name] [quantity] [supplier_id]";
    public static final String CREATE_SUPPLIER = "5. Create supplier - command: create_supplier [name]";
    public static final String DELETE_PRODUCT = "6. Delete product - command: delete_product [product_id]";
    public static final String DELETE_SUPPLIER = "7. Delete supplier - command: delete_supplier [supplier_id]";
    public static final String UPDATE_PRODUCT = "8. Update product - command: update_product [name] [quantity] " +
            "[product_id]";
    public static final String UPDATE_SUPPLIER = "9. Update supplier - command: update_supplier [name] [supplier_id]";

    public void next(UserInteractionContext context) {
        prepareMessage();
        context.setState(new Welcome());
    }

    public void prepareMessage() {
        printMessage(WELCOME);
        printMessage(LIST_OF_OPTIONS);
        printMessage(SUPPLIER_LIST);
        printMessage(SUPPLIER_DETAILS);
        printMessage(PRODUCT_LIST);
        printMessage(CREATE_PRODUCT);
        printMessage(CREATE_SUPPLIER);
        printMessage(DELETE_PRODUCT);
        printMessage(DELETE_SUPPLIER);
        printMessage(UPDATE_PRODUCT);
        printMessage(UPDATE_SUPPLIER);
    }
}
