package com.chkris.interaction;

public abstract class State {

    abstract void next(UserInteractionContext context);

    protected void printMessage(String message) {
        System.out.println(message);
    }
}
