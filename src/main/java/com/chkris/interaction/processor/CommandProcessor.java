package com.chkris.interaction.processor;

import com.chkris.entity.Model;
import com.chkris.interaction.State;
import com.chkris.interaction.Welcome;

public class CommandProcessor {

    public static final String COMMAND_SUPPLIER_LIST = "supplier_list";

    public static final String DELIMITER = " ";

    private State state;

    private Model model;

    public State getState() {
        return state;
    }

    public Model getModel() {
        return model;
    }

    public void process(String input) {
        String[] commandParts = input.split(DELIMITER);

        if (commandParts[0].equals(COMMAND_SUPPLIER_LIST)) {
            //print here view of supplier list
            System.out.println("supplier list :)");
            state = new Welcome();
            model = null;
        }
    }
}
