package com.chkris.services;

import com.chkris.entity.Supplier;

import javax.persistence.EntityManager;

public class SupplierStorageManager {

    private EntityManager entityManager;

    public SupplierStorageManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void create(Supplier supplier) {
        this.entityManager.persist(supplier);
    }

    public void delete(Supplier supplier) {
        this.entityManager.remove(supplier);
    }

    public void update(Supplier supplier) {
        this.entityManager.merge(supplier);
    }
}
